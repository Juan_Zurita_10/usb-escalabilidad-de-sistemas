# Relational database scaling 
 
## Running steps
### 1. Start containers

Run the docker-compose file to initiate the containers: 
```bash
cd docker/
docker compose up -d
```
When initiating the master runs the `init.sql` file, so it contains data. You can verify that running: 
```bash
docker exec -it mysql-master mysql -u root -p 
```

and then: 
```sql
USE mydatabase; 
SELECT * FROM user, 
```
It should show something like that:

![alt text](res/users.png)

### 2. Configure the containers for replication.

**This will configure master**
```bash
docker exec -it mysql-master bash
apt update
apt install nano -y
nano /etc/mysql/my.cnf
``` 
On the config file, add this configuration
```text
skip-name-resolve
default_authentication_plugin = mysql_native_password
log-bin=mysql.00001
log-bin-index=123
server-id=1
binlog_format = ROW
```

Save the changes on exit from the master console

**This is the configuration of the repl1**
```bash
docker exec -it mysql-rep1 bash
apt update
apt install nano -y
nano /etc/mysql/my.cnf
``` 

and add this config: 
```text
skip-name-resolve
default_authentication_plugin = mysql_native_password
server-id=2
log-bin=mysql-replica-bin
relay-log=relay-log
binlog_format = ROW
```

Save an exit
**Similar step for rep2:**
```bash
docker exec -it mysql-rep2 bash
apt update
apt install nano -y
nano /etc/mysql/my.cnf
``` 

and add this config: 
```text
skip-name-resolve
default_authentication_plugin = mysql_native_password
server-id=3
log-bin=mysql-replica-bin
relay-log=relay-log
binlog_format = ROW
```

Save an exit

To apply the changes the restart the docker-compose containers
```bash
docker-compose restart
```

### 3. Copy existing data 
Generate a dumpfile from master: 
```bash
docker exec -it mysql-master bash
mysqldump -u root -p –master-data –all-databases > data.sql
exit
docker exec -it mysql-master mysql -u root -p

```

```sql
mysql> UNLOCK TABLES;
```

Copy datadump to host.
```bash
docker cp mysql-master:/data.sql ./data.sql
```

If data.sql contains warning comment it, for this permissions will be needed.
```bash
sudo chmod 777 data.sql
```

Comment the first line
![alt text](res/datasql.png)

Copy from host to replicas.

```
docker cp ./data.sql mysql-rep1:/data.sql
docker cp ./data.sql mysql-rep2:/data.sql
```

Run the data.sql file on both replicas
```bash
docker exec -it mysql-rep1 bash
mysql -uroot -p < data.sql
mysql -uroot -p
```

View that data was stored: 
```sql
USE testdb; 
SELECT * FROM users; 
```

Repeat the same process in replica 2

```bash
docker exec -it mysql-rep2 bash
mysql -uroot -p < data.sql
mysql -uroot -p
```

```sql
USE testdb; 
SELECT * FROM users; 
```


### 4. Set up containers for replications

**Get the master replication file and position**
```bash
docker exec -it mysql-master mysql -u root -p
```

```sql
CREATE USER 'repl'@'%' IDENTIFIED BY 'pw';
GRANT REPLICATION SLAVE ON *.* TO 'repl'@'%';
FLUSH PRIVILEGES;
SHOW MASTER STATUS;
```
You shoud see something like that:
![alt text](res/master_status.png)

Remember the file and the position values, will need that for later.

**Set up repl1**
```bash
docker exec -it mysql-rep1 mysql -u root -p
```

Here add the values got from `SHOW MASTER STATUS;` from master in `MASTER_LOG_FILE` and `MASTER_LOG_POS` values
```sql
CHANGE MASTER TO MASTER_HOST='mysql-master', MASTER_USER='repl', MASTER_PASSWORD='pw', MASTER_LOG_FILE='mysql.000001', MASTER_LOG_POS=826;
```

Start the the slave
```mysql
START SLAVE;

SHOW SLAVE STATUS\G
```
You should see something like that
![alt text](res/slave_status_1.png)

Verify that `Slave IO Running` and `Slave SQL Running` are set to yes

**Set up rep2** 
```bash
docker exec -it mysql-rep2 mysql -u root -p
```

Here add the values got from `SHOW MASTER STATUS;` from master in `MASTER_LOG_FILE` and `MASTER_LOG_POS` values
```sql
CHANGE MASTER TO MASTER_HOST='mysql-master', MASTER_USER='repl', MASTER_PASSWORD='pw', MASTER_LOG_FILE='mysql.000001', MASTER_LOG_POS=826;
```

Start the the slave
```mysql
START SLAVE;

SHOW SLAVE STATUS\G
```
You should see something like that
![alt text](res/slave_status_1.png)

Verify that `Slave IO Running` and `Slave SQL Running` are set to yes

### 5. Test replication
Now everything is ready to work.

Insert data on master: 
```sql
USE testdb;
INSERT INTO users (name, email) VALUES (CONCAT('User', 10002), CONCAT('user', 10002, '@example.com'));
```

And you should be able too see it on the replicas: 
```sql
USE testdb;
select * from users where name = "User10002";
```