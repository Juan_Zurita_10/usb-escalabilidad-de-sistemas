USE testdb;

CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE
);

DELIMITER //

CREATE PROCEDURE AddUsers()
BEGIN
    DECLARE counter INT DEFAULT 1;
    DECLARE max_users INT DEFAULT 1000;
    WHILE counter <= max_users DO
        INSERT INTO users (name, email) VALUES (CONCAT('User', counter), CONCAT('user', counter, '@example.com'));
        SET counter = counter + 1;
    END WHILE;
END//

DELIMITER ;

CALL AddUsers();
